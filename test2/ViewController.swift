//
//  ViewController.swift
//  test2
//
//  Created by Gurlagan Bhullar on 2019-11-07.
//  Copyright © 2019 Gurlagan Bhullar. All rights reserved.
//

import UIKit
import Particle_SDK

class ViewController: UIViewController {

    @IBOutlet weak var showTimeLabel: UILabel!
    @IBOutlet weak var sliderValue: UISlider!
    
    @IBOutlet weak var valueSLiderr: UILabel!
    let USERNAME = "gurlaganbhullar@gmail.com"
    let PASSWORD = "12345"
    
    let DEVICE_ID = "420031000f47363333343437"
    var myPhoton : ParticleDevice?
    var countTime = 0
    var speedValue = 50;
    var valueOfSlider = 1
   
    var timerCount = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        // 1. Initialize the SDK
        ParticleCloud.init()
        // 2. Login to your account
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                // Something went wrong!
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                // Print out more detailed information
                print(error?.localizedDescription)
            }
            else {
                print("Login success!")
                
                // try to get the device
                self.getDeviceFromCloud()
                
            }
        }
    }
    func getDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription)
                return
            }
            else {
                print("Got photon from cloud: \(device?.id)")
                self.myPhoton = device
            }
            
        } // end getDevice()
    }

    func beginCounter(){
        timerCount = Timer.scheduledTimer(timeInterval: 1.0 , target: self, selector: #selector(countDownTimer), userInfo: nil, repeats: true)
    }
    @objc func countDownTimer(){
        if(countTime < 20){
        self.countTime = self.countTime + 1
            print(countTime)
        showTimeLabel.text = String(countTime)
        }
 
    }

    @IBAction func startButtonPressed(_ sender: Any) {
        beginCounter()
        faceEmoji(time: self.valueOfSlider)
        
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        self.valueOfSlider = Int(sender.value)
        valueSLiderr.text = String(valueOfSlider)
        print(valueOfSlider)
    }
    func faceEmoji(time : Int){
        speedValue = 50 * valueOfSlider
        print(speedValue)
        let parameters = ["\(speedValue)"]
        var task = myPhoton!.callFunction("faceEmoji", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle")
            }
            else {
                print("Error in green function")
            }
            
        }
        }
    }

//showFace(time: self.count)
//timeInterval: TimeInterval(self.sliderValue),
